# IndovinaTORI #

### Requirements ###

- [Node](https://docs.npmjs.com/getting-started/installing-node)
- [Cordova](https://cordova.apache.org/#getstarted)
- [Grunt](http://gruntjs.com/installing-grunt)

### Setup the project ###

After cloning the repository, run from terminal:

    npm install

This will install all modules listed in the **package.json** file.

### Project structure ###

The structure is the following:

- **www_prod**: production www folder, containing the app code
- **platforms**: directory that contains the specified app project
- **plugins**: folder containing all used Cordova plugins
- **config.xml**: file that describe the general project properties
- **gruntfile.js**: grunt script
- **package.json**: file that lists used package

### How to modify the project ###

The only folder that needs to be manually modified is the **www_prod** folder. After have been applied the changes to this folder, run the command:

    grunt prepareProject

or its shortcut

    grunt pp

This command will create/update the **www** folder minifying all its files and will build with Cordova all the available platforms.

If you want to run the project on a specify platform (emulator or device), run:

    cordova run platform

### Adding game levels ###

If you want to add game levels, you need to follow 2 easy steps:

- Add image_name.png to **www_prod/img/tori/** folder
- Add a line to **www_prod/js/imgs.js** following the model


```
#!javascript

["NAME","./img/tori/name." + ext]

```

**REMEMBER:** being an associative array, all lines end with a ',' character except for the last one!