module.exports = function(grunt) {

    /************************* GLOBAL CONFIG ***************************/
    grunt.initConfig({
        cwproj: '',
        app: {
          name: 'IndovinaTORI'
        },
        copy: {
            www: {
                expand: true,
                cwd: 'www_prod',
                src: '**',
                dest: 'www'
            }
        },
        clean: {
            www: ['www/index_test.html']
        },
        uglify: {
            options: {
                mangle: false
            },
            www: {
              files: [{
                  expand: true,
                  cwd: 'www/js',
                  src: '**/*.js',
                  dest: 'www/js'
              }]
            }
        },
        cssmin: {
            www: {
                files: [{
                    expand: true,
                    cwd: 'www/css',
                    src: ['*.css', '*.css'],
                    dest: 'www/css'
                }]
            }
        },
        htmlmin: {
            www: {
                options: {
                    removeComments: true,
                    collapseWhitespace: true
                },
                files: {
                    'www/index.html': 'www/index.html'
                }
            }
        },
        cordovacli: {
            options: {
                path: '.',
                cli: 'cordova'
            },
            cordova: {
                options: {
                    command: ['build'],
                    platforms: ['ios','android'],
                    path: '.',
                    id: 'com.indovinatori',
                    name: 'IndovinaTORI'
                }
            },
            build_ios: {
                options: {
                    command: 'build',
                    platforms: ['ios']
                }
            },
            build_android: {
                options: {
                    command: 'build',
                    platforms: ['android']
                }
            }
        }
    });

    /*********************** LOAD TASKS *************************/

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-cordovacli');
    grunt.loadNpmTasks('grunt-shell');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');

    /******************** REGISTER TASKS ************************/

    grunt.registerTask('pp', 'Prepare project shortcut', function() {
        grunt.task.run('prepareProject');
    });

    grunt.registerTask('prepareProject', 'Prepare www folder deleting useless file, minify js and css files', function() {
        grunt.task.run('copy:www');
        grunt.task.run('clean:www');
        grunt.task.run('uglify:www');
        grunt.task.run('cssmin:www');
        grunt.task.run('htmlmin');
        grunt.task.run('cordovacli:build_android');
        grunt.task.run('cordovacli:build_ios');
    });

    grunt.registerTask('hello', 'I am alive!', function(who) {
        if(who) grunt.log.write('Hi ' + who);
        else grunt.log.write('Hi, what\'s your name?');
    });

  grunt.registerTask('default', ['prepareProject']);

};
