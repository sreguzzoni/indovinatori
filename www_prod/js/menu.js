function startClick() {
    execAsync(showSection("game","menu"));
    resetGame();
}

function continueClick() {
    execAsync(showSection("game","menu"));
    continueGame();
}

// called by app.js only once
function loadMenu() {
    $("#menu_start").bind(eventClick, function() {
        if(localStorage.getItem("isGameStarted") == 1) {
            // if there is a game already started
            continueClick();
        } else {
            startClick();
        }
    });
}
