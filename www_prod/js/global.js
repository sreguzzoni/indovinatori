// CONST
var DEBUG = true;
var BROWSER;
var ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
var LETTER_SCORE = 10;
var VERSION = "1.0.0";
var HEIGHT;

// global vars
var imgs = initImgs();
var strangledIndex = 0;
var wordChoosen;
var number = -1;
var appLoaded = false;
var score = 0;
var eventClick = "touchstart";
var isChanging = false;
var cws = "menu";
var sections = new Array();
var backgroundMusicPath = "sounds/bg_music.wav";
var rightSoundPath = "sounds/right.wav";
var wrongSoundPath = "sounds/wrong.mp3";


/*************** GLOBAL FUNCTIONS ****************/

// show OS native modals
function showAlert(message, callback, title, buttonName) {
	if(!BROWSER) {
		navigator.notification.alert(message, callback, title, buttonName);
		AndroidFullScreen.immersiveMode();
	} else if(BROWSER) {
		alert(message);
		callback();
	}
}

// hide all sections, show selected section
function showSection(section, from) {
	// if is not changing
	if(!isChanging) {
		isChanging = true;
		// pick current section
		cws = section;
		// animate container
		$("#"+from+"_container").animate(
			{
	  			translateY: '-' + (1.5*HEIGHT) + 'px'
			},
			750,
			'ease-out',
			function() {
				// when the animation is finished, hide the from container and put z-index to 2 at the currently showed container
				$("#"+from+"_container").hide();
				$("#"+section+"_container").css("z-index","2");
				$("#"+from+"_container").css("z-index","0");
				// reposition the container at the bottom
				$("#"+from+"_container").animate(
					{
						translateY: '0px',
					},
					1,
					'ease-out',
					function() {
						isChanging = false;
					}
				);
			}
		);
		// show the container
		$("#"+section+"_container").show();
	}
}

/**************** Previuos version with HTML sections filled dynamically *******************
// function that fills the sections array var with all containers' HTML for speed up rendering
function loadHtmlContainers() {
	$(".container").each(function() {
		sections[$(this).attr("id")] = $(this).html();
	});
	$(".container").html("");
}
*/

function execAsync(func) {
	setTimeout(func,0)
}
