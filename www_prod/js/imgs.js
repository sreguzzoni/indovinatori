function initImgs(){
	// image format
	var ext = "png";
	// image map
	var imgs =
	[
		["ACCONCIATORI","./img/tori/acconciatori." + ext],
		["ADDOBBATORI","./img/tori/addobbatori." + ext],
		["AMATORI","./img/tori/amatori." + ext],
		["AMPLIFICATORI","./img/tori/amplificatori." + ext],
		["CALCIATORI","./img/tori/calciatori." + ext],
		["CERCATORI","./img/tori/cercatori." + ext],
		["GUIDATORI","./img/tori/guidatori." + ext],
		["IMITATORI","./img/tori/imitatori." + ext],
		["INDICATORI","./img/tori/indicatori." + ext],
		["IPNOTIZZATORI","./img/tori/ipnotizzatori." + ext],
		["OSSERVATORI","./img/tori/osservatori." + ext],
		["PARCHEGGIATORI","./img/tori/parcheggiatori." + ext],
		["PUNTATORI","./img/tori/puntatori." + ext],
		["RAPINATORI","./img/tori/rapinatori." + ext],
		["RIMORCHIATORI","./img/tori/rimorchiatori." + ext],
		["SALDATORI","./img/tori/saldatori." + ext],
		["SEMINATORI","./img/tori/seminatori." + ext],
		["SEQUESTRATORI","./img/tori/sequestratori." + ext],
		["SOLLEVATORI","./img/tori/sollevatori." + ext],
		["TRASPORTATORI","./img/tori/trasportatori." + ext],
		["VINCITORI","./img/tori/vincitori." + ext]
	];
	return imgs;
}
