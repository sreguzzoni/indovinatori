if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/)) {
	// if mobile
	BROWSER = false;
    document.addEventListener("deviceready", onReady, false);

	if(!BROWSER) {
		document.write("<script src='cordova.js'></script>");
	}

} else {
	// if browser
	BROWSER = true;
	eventClick = "click";
    $(document).ready(onReady);
}

// load function
function onReady() {
	loadApp();
}

function onBackButton() {
	switch (cws) {
		case "game":
			showSection("menu",cws);
			break;
		case "options":
			showSection("game",cws);
			break;
		case "menu":
			navigator.app.exitApp();
			break;
	}
}

function onMenuButton() {
	switch (cws) {
		case "game":
			showSection("options",cws);
			break;
	}
}

// load for the first time the app
function loadApp() {
	// init music ls item
	if(window.localStorage.getItem("music") != "0") {
		window.localStorage.setItem("music","1");
	}
	// if first time
	if(!appLoaded) {
		if(!BROWSER) {
			var platform = device.platform;
			if (platform == "Android") {
				// manage native events
				document.addEventListener("backbutton", onBackButton, false);
				navigator.app.overrideButton("menubutton", true);
				document.addEventListener("menubutton", onMenuButton, false);
			}
			document.addEventListener("resume", function() { StatusBar.hide(); }, false);
			// manage splashscreen
			navigator.splashscreen.show();
			window.setTimeout(function () {
	    		navigator.splashscreen.hide();
				window.setTimeout(function () {
					// hide statusbar
					StatusBar.hide();
					// load bg loop music
					window.plugins.NativeAudio.preloadComplex("bg_music", backgroundMusicPath, 1, 1, 0,
						function(msg){
							window.plugins.NativeAudio.loop("bg_music");
						},
						function(msg){
							console.log(msg);
						}
					);
					// load keyboard sounds
					window.plugins.NativeAudio.preloadSimple("right", rightSoundPath,
						function(msg){console.log(msg);},
						function(msg){console.log(msg);}
					);
					window.plugins.NativeAudio.preloadSimple("wrong", wrongSoundPath,
						function(msg){console.log(msg);},
						function(msg){console.log(msg);}
					);
				}, 700);
			}, 4000);
		} else {
			// load browser sounds and music
			Audio.prototype.stop = function()
			{
    			this.currentTime = 0.0;
				return true;
			}
			window.backgroundMusic = new Audio(backgroundMusicPath);
			window.rightSound = new Audio(rightSoundPath);
			window.wrongSound = new Audio(wrongSoundPath);
			// loop bgmusic
			window.backgroundMusic.addEventListener('ended', function() {
				this.currentTime = 0;
				this.play();
			}, false);
			if(window.localStorage.getItem("music") == "1")
				window.backgroundMusic.play();
		}
		// FastClick lib attachment
		$(function() {
    		FastClick.attach(document.body);
		});
		showSection("menu");


		loadSections();
		// save HEIGHT after rendered the first app's section
		HEIGHT = $("body").height();
		appLoaded = true;
	}
}

// load all the sections only once
function loadSections() {
	loadMenu();
	loadGame();
	loadOptions();
	loadEnd();
}
