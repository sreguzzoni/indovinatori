// load a single round
function loadRound(){
	var wordLength = loadRandomImg();
	if(wordLength == "endGame") {
		endGame(1);
	} else {
		// set spaces for word
		loadAdvise(wordLength);
		// fill spaces with letters
		loadAdviseTori(wordLength);
		// replace all right/wrong letters with normal ones
		for (i=0; i<ALPHABET.length; i++) {
			localStorage.setItem(ALPHABET[i], 0);
		}
		clearKeyboard();
	}
}

// reset the game
function resetGame() {
	// reload round
	loadRound();
	// set generic game parameters
	localStorage.setItem("level",0);
	for (i=0; i<imgs.length; i++) {
		localStorage.setItem("number_"+i, 0);
	}
	// write score
	localStorage.setItem("score",0);
	score = 0;
	updateScore();
	// reset strangled item
	strangledIndex=0;
	localStorage.setItem("strangledIndex", 0);
	loadStrangledImg();
	localStorage.setItem("isGameStarted", 1);
}

function continueGame() {
	number = localStorage.getItem("number");
	score = parseInt(localStorage.getItem("score"));
	strangledIndex = localStorage.getItem("strangledIndex");
	wordChoosen = imgs[number][0];
	loadAdvise(wordChoosen.length);
	loadAdviseTori(wordChoosen.length);
	loadStrangledImg();
	updateScore();
	for (i=0; i<ALPHABET.length; i++) {
		var val = localStorage.getItem(ALPHABET[i]);
		// if right letter
		if(val == 1) {
			var letterImg = $("img[value='" + ALPHABET[i].toLowerCase() + "']");
			letterImg.attr("src","img/chars/right.png");
			letterImg.attr("right", "true");
			letterImg.addClass("right");
			// show already got letters
			for(var j = 0; j<wordChoosen.length-4; j++) {
				if(ALPHABET[i] == wordChoosen[j]) {
					$("#advise .normal :eq(" + j + ")").text(ALPHABET[i]);
				}
			}
		} else if (val == -1) { //if wrong letter
			var letterImg = $("img[value*='" + ALPHABET[i].toLowerCase() + "']");
			letterImg.attr("src","img/chars/wrong.png");
			letterImg.attr("wrong", "true");
			letterImg.addClass("wrong");
		}
	}
	$("#image_selected").attr("src",imgs[number][1]);
}

// load a random img, returning as value the associated word length or the string "endGame"
function loadRandomImg(){
	// if the player has completed the game
	if(localStorage.getItem("level") == imgs.length){
		return "endGame";
	} else {
		number = 0 + Math.floor(Math.random() * imgs.length);
		while(localStorage.getItem("number_"+number) == 1) {
			number = 0 + Math.floor(Math.random() * imgs.length);
		}
		//load image and store data in localStorage
		imgChoosen = imgs[number];
		wordChoosen = imgChoosen[0];
		localStorage.setItem("number",number);
		$("#image_selected").attr("src",imgChoosen[1]);
		// return word length
		return wordChoosen.length;
	}
}

function loadStrangledImg(){
	if(strangledIndex == 3) {
		/*showAlert(
	    	'Hai perso!',		// message
	    	function() {
				endGame(0);
			},			// callback
	    	'Game Over',		// title
	    	'Torna al menu'		// buttonName
		);*/
		endGame(0);
	} else {
		$("#game_bottom").css("background-image","url('img/game/game_bottom_" + strangledIndex + ".png')");
	}
}

// load the advise bar
function loadAdvise(length){
	$("#advise .normal").remove();
	var adviseTemplate = $("#advise .template");
	for(i=0; i<length; i++) {
		var newLetter = adviseTemplate.clone();
		newLetter.removeClass("template");
		newLetter.addClass("normal");
		$("#advise_word").append(newLetter);
	}
	var widthMax = $("#advise").width()*0.91; // % of width of advise div
	//-2% for left the space between the single chars
	$(".advise_letter").css("width",((widthMax/length)-widthMax*0.02));
}

/*
// ******************** Previuos version with "Try" button ****************************
function onClickLetter(containerLetter) {
	var isDisabled = ( ($(containerLetter).find("img").attr("right")=="true") || ($(containerLetter).find("img").attr("wrong")=="true") );
	// add/remove selected class
	var isSelected = $(containerLetter).find("img").hasClass("selected");
	if(!isSelected && !isDisabled) {
		$(containerLetter).find("img").addClass("selected");
	} else {
		$(containerLetter).find("img").removeClass("selected");
	}
}

function onClickConfirm(){
	// count the letters
	var selectedLetters = $(".selected");
	var strLength = selectedLetters.length;
	if(strLength > 0){
		tryNumber++;
		var isError = false;
		// check if there is at least an error
		$(selectedLetters).each(function() {
			var thisLetter = $(this).attr("value").toUpperCase();
			var wrongLetter = true;
			for(var i = 0; i<wordChoosen.length-4 ; i++) {
				if(thisLetter == wordChoosen[i]) {
					wrongLetter = false;
				}
			}
			if (wrongLetter) {
				isError = true;
			}
		});
		// if there aren't any errors, show the right letters
		if(!isError) {
			$(selectedLetters).each(function() {
				var thisLetter = $(this).attr("value").toUpperCase();
				for(var i = 0; i<wordChoosen.length-4 ; i++) {
					if(thisLetter == wordChoosen[i]) {
						$(this).attr("src","img/chars/right.png");
						$(this).attr("right", "true");
						$(this).addClass("right");
						$("#advise .normal :eq(" + i + ")").text(thisLetter);
					}
				}
			});
			// check if the user won the round
			if(getAllWordUser() == wordChoosen) {
				//if first try
				if(tryNumber == 1) {
					showAlert(
				    	'Complimenti!',
						function() {
							endRound(1);
						},
				    	'Indovinato al primo tentativo!',
				    	'Continua'
					);
				} else {
					showAlert(
				    	'Round compleato',
				    	function() {
							endRound(1);
						},
				    	'Congratulazioni!',
				    	'Continua'
					);
				}
				//playerWin();
			}
		} else {
			// if error
			if(strLength == 1) {
				// set wrong letter and disable it
				$(selectedLetters).attr("src","img/chars/wrong.png");
				$(selectedLetters).attr("wrong", "true");
				$(selectedLetters).addClass("wrong");
			}
			showAlert(
				'Hai sbagliato',
				function() {
					strangledIndex++;
					loadStrangledImg();
				},
				'Errore',
				'Continua'
			);
		}
	} else {
		showAlert(
			"Selezionare almeno una lettera",
			function() {},
			"Attenzione",
			"Continua"
		);
	}
	//remove all class "selected" from keyboard
	$(".selected").removeClass("selected");
}
*/

function onClickLetter(containerLetter) {
	// prevent button click if sections are changing
	if(!isChanging) {
		var isDisabled = ( ($(containerLetter).find("img").attr("right")=="true") || ($(containerLetter).find("img").attr("wrong")=="true") );
		if(!isDisabled) {
			tryLetter(containerLetter);
		}
	}
}

function tryLetter(containerLetter){
	var letterImg = $(containerLetter).find("img");
	var letter = letterImg.attr("value").toUpperCase();
	var isError = true;
	var scoreRound = 0;
	// check if the letter selected is wrong or right, showing it in the top bar
	for(var i = 0; i<wordChoosen.length-4 ; i++) {
		if(letter == wordChoosen[i]) {
			isError = false;
			$("#advise .normal :eq(" + i + ")").text(letter);
			scoreRound += LETTER_SCORE;
		}
	}
	localStorage.setItem(letter.toUpperCase(), !isError?1:-1);
	// if right
	if(!isError) {
		// sound function
		if(window.localStorage.getItem("music") == "1") {
			if(!BROWSER) {
				window.plugins.NativeAudio.play("right");
			} else {
				if(window.rightSound.stop())
					window.rightSound.play();
			}
		}
		// show right char in the word, change btn
		letterImg.attr("src","img/chars/right.png");
		letterImg.attr("right", "true");
		letterImg.addClass("right");
		// update score
		addScore(scoreRound);
		updateScore();
		// check if the user won the round
		if(getAllWordUser() == wordChoosen) {
			/*showAlert(
		    	'Round compleato',
		    	function() {
					endRound(1);
				},
		    	'Congratulazioni!',
		    	'Continua'
			);
			*/
			endRound(1);
		}
	} else { // if error
		// sound function
		if(window.localStorage.getItem("music") == "1") {
			if(!BROWSER) {
				window.plugins.NativeAudio.play("wrong");
			} else {
				window.wrongSound.play();
			}
		}
		letterImg.attr("src","img/chars/wrong.png");
		letterImg.attr("wrong", "true");
		letterImg.addClass("wrong");
		/*showAlert(
			'Hai sbagliato',
			function() {
				strangledIndex++;
				localStorage.setItem("strangledIndex",strangledIndex);
				loadStrangledImg();
			},
			'Errore',
			'Continua'
		);*/
		strangledIndex++;
		localStorage.setItem("strangledIndex",strangledIndex);
		loadStrangledImg();
	}
}

// write "TORI" in the last 4 spaces of advise bar
function loadAdviseTori(wordLength){
	for(var i = wordLength; i>wordLength-4 ; i--) {
		$("#advise .normal :eq(" + (i-1) + ")").text(wordChoosen[i-1]);
	}
}

// return the pieces of word shown in the advise bar
function getAllWordUser(){
	var stringWord = "";
	$(".normal").each(function(){
		stringWord += $(this).html();
	});
	return stringWord;
	//JSON.stringify($(".normal"));
}

function endRound(how){
	localStorage.setItem("number_"+number,how);
	localStorage.setItem("level",parseInt(localStorage.getItem("level"))+1);
	loadRound();
}

function clearKeyboard(){
	$(".right").each(function(){
		$(this).attr("src","img/chars/" + $(this).attr("value") + ".png");
		$(this).attr("right","false");
		$(this).removeClass("right");
	});
	$(".wrong").each(function(){
		$(this).attr("src","img/chars/" + $(this).attr("value") + ".png");
		$(this).attr("wrong","false");
		$(this).removeClass("wrong");
	});
}

function addScore(number) {
	var thisInc = $("#game_score_inc").clone();
	thisInc.appendTo("#game_score");
	thisInc.text("+" + number);
	thisInc.css("display","block");
	thisInc.animate(
		{
  			translateY: '-30px',
			opacity: 0
		},
		700,
		'ease-out',
		function() { $(thisInc).remove(); }
	);

	score += number;
	localStorage.setItem("score", score);
}

function updateScore() {
	$("#game_score_num").text(score);
}

// resize advise font
function resizeFont() {
  var ww = $('body').width();
  ww = (ww/100) * 18;
  $(".advise_letter").css('font-size',ww+'%');
}

// called by app.js only once
function loadGame() {
	// load generic app game functions
	resizeFont();
	$(".letter_button").bind(eventClick, function() { onClickLetter($(this)); });
	$("#game_settings").bind(eventClick, function() { showSection("options", "game"); });
}

// return true if the score has been updated
function updateMaximumScore() {
	var record = 0;
	if(localStorage.getItem("maximumScore")) {
		record = parseInt(localStorage.getItem("maximumScore"));
	}
	if(score > record) {
		localStorage.setItem("maximumScore",score);
		return true;
	}
	return false;
}
