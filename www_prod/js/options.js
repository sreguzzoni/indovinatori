function loadOptions() {
    loadMusicImage();
    $("#options_menu").bind(eventClick, function() { showSection("menu","options"); });
    $("#options_continue").bind(eventClick, function() { showSection("game","options"); continueGame(); });
    $("#options_replay").bind(eventClick, function() { showSection("game","options"); resetGame(); });
    $("#options_music").bind(eventClick, function() {
        if(window.localStorage.getItem("music") == "0") {
            // able music
            window.localStorage.setItem("music","1");
            if(!BROWSER) {
                window.plugins.NativeAudio.loop("bg_music");
            } else {
                window.backgroundMusic.play();
            }
            /********** TEMP needs to implement mobile function ******/
        } else {
            // disable music
            window.localStorage.setItem("music","0");
            if(!BROWSER) {
                window.plugins.NativeAudio.stop("bg_music");
            } else {
                window.backgroundMusic.pause();
            }
            /********** TEMP needs to implement mobile function ******/
        }
        loadMusicImage();
    });
}

function loadMusicImage() {
    if( window.localStorage.getItem("music") == "1" ) {
        $("#options_music").attr("src","img/options/options_music.png");
    } else {
        $("#options_music").attr("src","img/options/options_music_off.png");
    }

}
