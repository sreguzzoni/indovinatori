var endMessageDescNewScore = "Nuovo punteggio massimo di";
var endMessageDescNotNewScore = "Punteggio massimo:";

function endGame(how) {
    // show section
    showSection("end","game");
    var isNewRecord = updateMaximumScore();
	// game lost
	if(how == 0) {
        $("#end_center").removeClass("won");
        $("#end_center").addClass("lost");
        $("#end").css("background-image","none");
	} else { // game won
        $("#end_center").removeClass("lost");
        $("#end_center").addClass("won");
        $("#end").css("background-image","url('./img/end/end_sun.png')");
	}
    // show new record or not
    if(isNewRecord) {
        $("#end_message_desc").text(endMessageDescNewScore);
    } else {
        $("#end_message_desc").text(endMessageDescNotNewScore);
    }
    $("#end_message_score").text(localStorage.getItem("maximumScore"));
    setTimeout(resetGame, 150);
}

function loadEnd() {
    $("#end_replay").bind(eventClick, function() { showSection("game","end"); resetGame(); });
    $("#end_menu").bind(eventClick, function() { showSection("menu","end"); });
}
